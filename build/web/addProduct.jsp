<%-- 
    Document   : addProduct
    Created on : Nov 17, 2020, 5:02:14 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Add Product Page</title>
    </head>
    <body>
        <div class="container">
            <div class="navbar">
                <div class="logo">
                    <img src="images/logo.png" width="125px">
                </div>
                <nav>
                    <ul id="MenuItems"> 
                        <li><a href="Home">Home</a></li>
                        <li><a href="allProduct">Product</a></li>
                        <li><a href="">About</a></li>
                        <li><a href="">Contact</a></li>
                        <li><a href="logout">Log Out</a></li>
                    </ul>
                </nav>
                <a href="CartServlet"><img src="images/cart.png" width="30px" height="30px"></a>
                <img src="images/menu.png" class="menu-icon" onclick="menutoggle()">

            </div>
        </div>
        <div class="add-page my-5 text-center" style="background-color: #ff523b; color: #fff;">
            <h1><strong>Add a new Product</strong></h1>
        </div>
        <form action="addProduct" method="post">
            <table class="table table-borderless col-sm-12 mx-2 ">
                <tr>
                    <td >
                        <h4 class="text-left"><strong>PRODUCT ID</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="id" placeholder="Enter ID" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>PRODUCT NAME</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="name" placeholder="Enter Name" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>PRODUCT DESCRIPTION</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="description" placeholder="Enter Description" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>PRICE</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="number" name="price" placeholder="Enter Price" style ="width: 100%">
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>PRODUCT TYPE</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <select name="type" style="border-color: #000; width: 100%">
                            <option value="SHOES">Shoes</option>
                            <option value="CLOTHING">Clothing</option>
                            <option value="ACCESSORIES">Accessories</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4 class="text-left"><strong>IMAGES</strong></h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="file" name="img" placeholder="Images" style ="width: 100%">
                    </td>
                </tr>
            </table>
            <div class="submit-btn mx-2 my-4" style="text-align: center; align-items: center">
                <input type="submit" name="btnUpdate" value="Update" class="btn my-0 col-2">
            </div>
        </form>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col-1">
                        <h3>Download Our App</h3>
                        <p>Download App For Android and IOS Mobile Phone.</p>
                        <div class="app-logo">
                            <img src="images/play-store.png">
                            <img src="images/app-store.png">
                        </div>
                    </div>
                    <div class="footer-col-2">
                        <img src="images/logo-white.png" >
                        <p>Our Purpose is Subtainably Make The Pleasure and Benefits Of Sports Accessible to the Many.</p>
                    </div>
                    <div class="footer-col-3">
                        <h3>Useful Links</h3>
                        <ul>
                            <li>Coupons</li>
                            <li>Blog Post</li>
                            <li>Return Policy</li>
                            <li>Join Affiliate</li>
                        </ul>
                    </div>
                    <div class="footer-col-4">
                        <h3>Follow Us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggle() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    </body>
</html>

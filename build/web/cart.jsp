<%-- 
    Document   : cart
    Created on : Nov 4, 2020, 10:39:09 PM
    Author     : Admin
--%>

<%@page import="java.math.BigDecimal"%>
<%@page import="Model.Product"%>
<%@page import="DAO.ProductDAO"%>
<%@page import="Model.Inventory"%>
<%@page import="java.util.List"%>
<%@page import="Model.LineProduct"%>
<%@page import="Model.ShoppingCart"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cart</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    </head>
    <body>
        <!-- navigation bar -->
        <div class="navbar">
            <div class="logo">
                <img src="images/logo.png" width="125px">
            </div>
            <nav>
                <ul id="MenuItems"> 
                    <li><a href="Home">Home</a></li>
                    <li><a href="allProduct">Product</a></li>
                    <li><a href="">About</a></li>
                    <li><a href="">Contact</a></li>
                    <li><a href="logout">Log Out</a></li>
                </ul>
            </nav>
            <img src="images/cart.png" width="30px" height="30px">
            <img src="images/menu.png" class="menu-icon" onclick="menutoggle()">
            
        </div>
        <!-- cart items details -->
        <div class="small-container cart-page">


            <%
                if (session.getAttribute("cart") == null) {
            %>
                <h2>
                    ${requestScope.msg}
                    <br>
                    Your cart is empty now!    
                </h2>
            <%
                }
            %>


            <%
                if (session.getAttribute("cart") != null) {
                    ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
                    BigDecimal totalPrice = new BigDecimal("0");
                    // load lineitem in cart
                    List<LineProduct> lps = cart.listItemsInCart();
                    //load inventory
                    if (lps.size() == 0) {
            %>
                <h2>
                    Your cart is empty now!    
                </h2>
            <%
            } else {
            %>
            <table class="cart-table">
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Subtotal</th>
                </tr>
                <%
                    Inventory inventory = new Inventory();
                    ProductDAO pd = new ProductDAO();
                    List<Product> products = pd.getProducts();
                    List<LineProduct> productsInCart;
                    for (Product p : products) {
                        inventory.add(p);
                    }
                    for (LineProduct lpInCart : lps) {
                        Product p = inventory.get(lpInCart.getProductId());
                        totalPrice = totalPrice.add(lpInCart.totalPrice());
                %>
                <tr>
                    <td>
                        <div class="cart-info">
                            <img src="<%=p.getImg()%>">
                            <div>
                                <p><%=p.getName()%></p>
                                <small>$<%=p.getPrice()%></small>
                                <br>
                                <a href="removeFromCart?productId=<%=p.getId()%>">Remove</a>
                            </div>
                        </div>
                    </td>
                    <td><input type="number" name="quantity" value="<%=lpInCart.getQuantity()%>"></td>
                    <td>$<%=lpInCart.totalPrice()%></td>
                </tr>
                <%
                    }
                %>
            </table>
            <div class = "total-price">
                <table>
                    <tr>
                        <td>Subtotal</td>
                        <td>$<%=totalPrice%></td>
                    </tr>

                    <tr>
                        <td>Tax</td>
                        <td>$<%=totalPrice.divide(BigDecimal.TEN)%></td>
                    </tr>
                    <tr>
                        <td>Total</td>
                        <td>$<%=totalPrice.multiply(new BigDecimal("1.1"))%></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <a class="btn" href="checkout.jsp?totalPrice=<%=totalPrice%>">Checkout</a>
                        </td>
                    </tr>
                </table>
            </div>           
            <%
                    }
                }
            %>



        </div>


        <!-- footer -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col-1">
                        <h3>Download Our App</h3>
                        <p>Download App For Android and IOS Mobile Phone.</p>
                        <div class="app-logo">
                            <img src="images/play-store.png">
                            <img src="images/app-store.png">
                        </div>
                    </div>
                    <div class="footer-col-2">
                        <img src="images/logo-white.png" >
                        <p>Our Purpose is Subtainably Make The Pleasure and Benefits Of Sports Accessible to the Many.</p>
                    </div>
                    <div class="footer-col-3">
                        <h3>Useful Links</h3>
                        <ul>
                            <li>Coupons</li>
                            <li>Blog Post</li>
                            <li>Return Policy</li>
                            <li>Join Affiliate</li>
                        </ul>
                    </div>
                    <div class="footer-col-4">
                        <h3>Follow Us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
                <p class="copyRight">Copyright 2020- Easy Tutorials</p>
            </div>
        </div>
        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggle() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>

    </body>
</html>

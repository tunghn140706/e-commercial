<%-- 
    Document   : home
    Created on : Nov 3, 2020, 10:28:47 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tung Store</title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
    </head>
    <body>

        <div class="header">
            <div class="container">
                <div class="navbar">
                    <div class="logo">
                        <img src="images/logo.png" width="125px">
                    </div>
                    <nav>
                        <ul id="MenuItems"> 
                            <li><a href="Home">Home</a></li>
                            <c:if test="${sessionScope.role eq 'ADMIN'}"><li><a href="alter">Alter Products</a></li></c:if>
                            <li><a href="allProduct">Product</a></li>
                            <li><a href="">About</a></li>
                            <li><a href="">Contact</a></li>
                            <li><a href="logout">Log Out</a></li>
                        </ul>
                    </nav>
                    <a href="CartServlet"><img src="images/cart.png" width="30px" height="30px"></a>
                    <img src="images/menu.png" class="menu-icon" onclick="menutoggle()">

                </div>
                <div class="row">
                    <div class="col-2">
                        <h1>Give Your Workout <br> A New Style</h1>
                        <p>Success isn't always about the greatness. It's about consistancy.
                            Consistant <br>hard work gains success. Greatness will come</p>
                        <a href="" class="btn">Explore Now &#10140</a>
                    </div>
                    <div class="col-2">
                        <img src="images/image1.png">
                    </div>
                </div>
            </div>
        </div>

        <!-- 	featured categories -->
        <div class="categories">
            <div class="small-container">
                <div class="col-3">
                    <img src="images/category-1.jpg">
                </div>
                <div class="col-3">
                    <img src="images/category-2.jpg">
                </div>
                <div class="col-3">
                        <img src="images/category-3.jpg">
                </div>
            </div>
        </div>

        <!-- 	featured products -->
        <div class="small-container1">
            <h2 class="title">Featured Products</h2>
            <div class="row">
                
                <c:forEach var="f" items="${requestScope.products}">
                <div class="col-4">
                    <a href="productDetails?id=${f.id}"><img src="${f.img}" ></a>
                    <h4>${f.name}</h4>
                    <div class="Rating">
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star" aria-hidden="true"></i>
                        <i class="fa fa-star-o" aria-hidden="true"></i>
                    </div>
                    <p>$${f.price}</p>
                </div>
                </c:forEach>
            </div>
        </div>
        <!-- 
                ------OFFER------ -->
        <div class="offer">
            <div class="small-container">
                <div class="row">
                    <div class="col-2">
                        <img src="images/exclusive.png"  class="offer-img">
                    </div>
                    <div class="col-2">
                        <p>Exclusively Available on TungStore</p>
                        <h1>Smart Band 4</h1>
                        <small>The Mi Smart Band 4 features a 39% larger AMOLED color
                            full-touch display with adjustable brightness. So everything is clear as it can be.<br></small>
                        <a href="" class="btn">Buy now &#10140</a>
                    </div>
                </div>
            </div>
        </div>


        <!-- 	--TESTIMONIAL-- -->
        <div class="testimonial">
            <div class="small-container">
                <div class="row">
                    <div class="col-3">
                        <i class="fa fa-quote-left"></i>
                        <p>Lorem Ipsum is simply dummy text of printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                        <i class="fa fa-quote-right"></i>
                        <div class="Rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <img src="images/user-1.png">
                        <h3>Seam Parker</h3>	
                    </div>
                    <div class="col-3">
                        <i class="fa fa-quote-left"></i>
                        <p>Lorem Ipsum is simply dummy text of printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                        <i class="fa fa-quote-right"></i>
                        <div class="Rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <img src="images/user-2.png">
                        <h3>Mike Smith</h3>	
                    </div>
                    <div class="col-3">
                        <i class="fa fa-quote-left"></i>
                        <p>Lorem Ipsum is simply dummy text of printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever.</p>
                        <i class="fa fa-quote-right"></i>
                        <div class="Rating">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star-o" aria-hidden="true"></i>
                        </div>
                        <img src="images/user-3.png">
                        <h3>Mabel Joe</h3>	
                    </div>
                </div>
            </div>
        </div>

        <!-- ---------brand------------ -->
        <div class="brand">
            <div class="row">
                <div class="col-5">
                    <img src="images/logo-godrej.png">
                </div>
                <div class="col-5">
                    <img src="images/logo-oppo.png">
                </div>
                <div class="col-5">
                    <img src="images/logo-coca-cola.png">
                </div>
                <div class="col-5">
                    <img src="images/logo-paypal.png">
                </div>
                <div class="col-5">
                    <img src="images/logo-philips.png">
                </div>
            </div>
        </div>

        <!-- ----------footer----------- -->
        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="footer-col-1">
                        <h3>Download Our App</h3>
                        <p>Download App For Android and IOS Mobile Phone.</p>
                        <div class="app-logo">
                            <img src="images/play-store.png">
                            <img src="images/app-store.png">
                        </div>
                    </div>
                    <div class="footer-col-2">
                        <img src="images/logo-white.png" >
                        <p>Our Purpose is Subtainably Make The Pleasure and Benefits Of Sports Accessible to the Many.</p>
                    </div>
                    <div class="footer-col-3">
                        <h3>Useful Links</h3>
                        <ul>
                            <li>Coupons</li>
                            <li>Blog Post</li>
                            <li>Return Policy</li>
                            <li>Join Affiliate</li>
                        </ul>
                    </div>
                    <div class="footer-col-4">
                        <h3>Follow Us</h3>
                        <ul>
                            <li>Facebook</li>
                            <li>Twitter</li>
                            <li>Instagram</li>
                            <li>Youtube</li>
                        </ul>
                    </div>
                </div>
                <hr>
            </div>
        </div>


        <!-- ---------- js for toggle menu----------- -->
        <script>
            var MenuItems = document.getElementById("MenuItems");
            MenuItems.style.maxHeight = "0px";
            function menutoggle() {
                if (MenuItems.style.maxHeight == "0px") {
                    MenuItems.style.maxHeight = "200px";
                } else {
                    MenuItems.style.maxHeight = "0px";
                }
            }
        </script>
    </body>
</html>

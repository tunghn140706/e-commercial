/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DAO.AccountDAO;
import DAO.ProductDAO;
import Model.Account;
import Model.Inventory;
import Model.Product;
import Model.ShoppingCart;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Admin
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AccountDAO ad = new AccountDAO();
        List<Account> accounts = new ArrayList<Account>();
        String user = "";
        String pass = "";
        if (request.getCookies() == null) {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            Cookie[] cookies = request.getCookies();

            for (Cookie c : cookies) {
                if (c.getName().equals("user")) {
                    user = c.getValue();
                }
                if (c.getName().equals("pass")) {
                    pass = c.getValue();
                }
            }
            accounts = ad.getAccounts();
            for (Account ac : accounts) {
                if (ac.getUsername().equals(user) && ac.getPassword().equals(pass)) {
//                    ShoppingCart cart = loadCart();
//                    HttpSession session = request.getSession();
//                    session.setAttribute("cart", cart);
//                    session.setAttribute("userId", ac.getId());
//                    response.sendRedirect("Home");

                    HttpSession session = request.getSession();
                    ShoppingCart cart = (ShoppingCart) session.getAttribute("cart");
//                    session.setAttribute("userId", ac.getId());
                    response.sendRedirect("Home");
                    return;
                }
            }
            request.getRequestDispatcher("login.jsp").forward(request, response);

        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        AccountDAO ad = new AccountDAO();
        List<Account> accounts = new ArrayList<Account>();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String remember = request.getParameter("checkbox");
        accounts = ad.getAccounts();
        Account acc = null;
        int userId = 0;
        boolean hasAccount = false;
        for (Account a : accounts) {
            if (username.equalsIgnoreCase(a.getUsername()) && password.equals(a.getPassword())) {
                hasAccount = true;
                acc = a;
                userId = a.getId();
                break;
            }
        }

        if (hasAccount) {
            if (remember != null) {
                Cookie cookieUser = new Cookie("user", username);
                Cookie cookiePass = new Cookie("pass", password);

                cookieUser.setMaxAge(60 * 60 * 24);
                cookiePass.setMaxAge(60 * 60 * 24);

                response.addCookie(cookieUser);
                response.addCookie(cookiePass);
            }
            ShoppingCart cart = loadCart();
            HttpSession session = request.getSession();
            String role = acc.getRole().name();
            session.setAttribute("role", role);
            session.setAttribute("userId", userId);
            session.setAttribute("cart", cart);
            
            response.sendRedirect("Home");
//            request.getRequestDispatcher("/Home").forward(request, response);
            return;
        }

        request.getRequestDispatcher("login.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public ShoppingCart loadCart() {
        Inventory inventory = new Inventory();
        List<Product> products = new ArrayList<Product>();
        ProductDAO pd = new ProductDAO();
        products = pd.getProducts();
        for (Product p : products) {
            inventory.add(p);
        }
        ShoppingCart cart = new ShoppingCart(inventory);
        return cart;
    }
}

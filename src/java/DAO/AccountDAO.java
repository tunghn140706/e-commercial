/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Account;
import Model.AccountType;
import connect.DBContext2;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class AccountDAO extends DBContext2{
    
    public List<Account> getAccounts(){
        List<Account> accounts = new ArrayList<Account>();
        try {
            String sql = "select * from Account";
            PreparedStatement st = con.prepareStatement(sql);  
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                a.setId(rs.getInt("ID"));
                a.setUsername(rs.getString("Username"));
                a.setPassword(rs.getString("Password"));
                a.setDescription(rs.getString("Description"));
                a.setRole(AccountType.valueOf(rs.getString("Role")));
                accounts.add(a);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }finally{
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return accounts;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Model.Product;
import Model.ProductType;
import connect.DBContext2;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ProductDAO extends DBContext2 {

    public List<Product> getProducts() {
        List<Product> products = new ArrayList<>();
        try {
            String sql = "select *\n"
                    + "from Product";
            PreparedStatement statement = con.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String name = rs.getString("name");
                ProductType productType = ProductType.valueOf(rs.getString("Type"));
                BigDecimal price = new BigDecimal(rs.getString("price"));
                String description = rs.getString("description");
                String img = rs.getString("image");
                Product p = new Product.ProductBuilder(id, name).withPrice(price).withProductType(productType)
                        .withDescription(description).withImg(img).build();
                products.add(p);
            }
        } catch (Exception e) {
            System.out.println("Loading products ERRORS: " + e.getStackTrace());;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return products;
    }

    public List<Product> getProductsPaging(int endPoint) {
        List<Product> products = new ArrayList<>();
        try {
            String sql = "with T as(\n"
                    + "	select top "+endPoint+"*\n"
                    + "	from Product\n"
                    + "	order by id asc\n"
                    + ")\n"
                    + "select top 8* from T\n"
                    + "order by id desc";
            PreparedStatement statement = con.prepareStatement(sql);
//            statement.setInt(1, endPoint);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String name = rs.getString("name");
                ProductType productType = ProductType.valueOf(rs.getString("Type"));
                BigDecimal price = new BigDecimal(rs.getString("price"));
                String description = rs.getString("description");
                String img = rs.getString("image");
                Product p = new Product.ProductBuilder(id, name).withPrice(price).withProductType(productType)
                        .withDescription(description).withImg(img).build();
                products.add(p);
            }
        } catch (Exception e) {
            System.out.println("Loading products ERRORS: " + e.getStackTrace());;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return products;
    }

    public Product getProductById(int id) {
        Product product = null;
        try {
            String sql = "select *\n"
                    + "from Product\n"
                    + "where id = ?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String ID = rs.getString("id");
                String name = rs.getString("name");
                ProductType productType = ProductType.valueOf(rs.getString("Type"));
                BigDecimal price = new BigDecimal(rs.getString("price"));
                String description = rs.getString("description");
                String img = rs.getString("image");
                product = new Product.ProductBuilder(ID, name).withPrice(price).withProductType(productType)
                        .withDescription(description).withImg(img).build();
            }
        } catch (Exception e) {
            System.out.println("Loading products ERRORS: " + e.getStackTrace());;
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return product;
    }

    public void deleteProduct(int id) {
        try {
            String sql = "DELETE FROM Product WHERE id=?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void updateProduct(Product s) {
        try {
            String sql = "UPDATE [Product]\n"
                    + "   SET [name] = ?\n"
                    + "      ,[price] = ?\n"
                    + "      ,[description] = ?\n"
                    + "      ,[image] = ?\n"
                    + "      ,[type] = ?\n"
                    + " WHERE [id] = ?";
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setString(1, s.getName());
            statement.setBigDecimal(2, s.getPrice());
            statement.setString(3, s.getDescription());
            statement.setString(4, s.getImg());
            statement.setString(5, s.getProductType().toString());
            statement.setString(6, s.getId());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void insertProduct(Product s) {
        try {
            String sql = "INSERT INTO [Product]\n"
                    + "           ([id]\n"
                    + "           ,[name]\n"
                    + "           ,[price]\n"
                    + "           ,[type]\n"
                    //                    + "           ,[img]\n"
                    + "           ,[description])\n"
                    + "     VALUES\n"
                    + "           (?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    + "           ,?\n"
                    //                    + "           ,?\n"
                    + "           ,?)";
            PreparedStatement statement = con.prepareStatement(sql);
//            statement.setInt(1, s.getId());
            statement.setString(1, s.getId());
            statement.setString(2, s.getName());
            statement.setString(3, s.getPrice().toPlainString());
            statement.setString(4, s.getProductType().toString());
//            statement.setString(5, s.getImg());
            statement.setString(5, s.getDescription());
            statement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}

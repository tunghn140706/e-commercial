/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Admin
 */
public class Account {
    private int id;
    private String username;
    private String password;
    private String description;
    private AccountType role;

    public Account(String username, String password, String description, AccountType type) {
        this.username = username;
        this.password = password;
        this.description = description;
        this.role = type;
    }

    public Account() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccountType getRole() {
        return role;
    }

    public void setRole(AccountType type) {
        this.role = type;
    }
    
}
